call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'bling/vim-airline'
Plug 'sjl/badwolf'
Plug 'kien/ctrlp.vim'

" Initialize plugin system
call plug#end()

colorscheme badwolf
syntax enable

set number
set showmatch

